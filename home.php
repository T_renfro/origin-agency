<?php
/**
 * Template Name: Home Page
 */
?>

<?php get_template_part('/templates/hero-section'); ?>
<?php get_template_part('/templates/featured-work'); ?>
<?php get_template_part('/templates/testimonials'); ?>
<?php get_template_part('/templates/team'); ?>
<?php get_template_part('/templates/contact'); ?>
