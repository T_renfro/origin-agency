<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h1>Team</h1>
        </div>
    </div>
    <div class="row">
        <?php if( have_rows('team_repeater') ): ?>
            <?php while( have_rows('team_repeater') ): the_row(); ?>
                <div class="col-sm-3">
                    <img class="img-responsive team-image"src="<?php echo get_sub_field('image'); ?>" />
                    <h2><?php echo get_sub_field('name'); ?></h2>
                    <p><?php echo get_sub_field('title'); ?></p>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
    </div>
</div>