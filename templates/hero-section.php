<div class="jumbotron">
    <div class="container">
        <h1><?php echo get_field('headline'); ?></h1>
        <p><?php echo get_field('description'); ?></p>
    </div>
</div>