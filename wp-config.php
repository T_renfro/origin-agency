<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */
 
// Include local configuration
if (file_exists(dirname(__FILE__) . '/local-config.php')) {
	include(dirname(__FILE__) . '/local-config.php');
}

// Global DB config
if (!defined('DB_NAME')) {
	define('DB_NAME', 'wp_oa');
}
if (!defined('DB_USER')) {
	define('DB_USER', 'root');
}
if (!defined('DB_PASSWORD')) {
	define('DB_PASSWORD', 'root');
}
if (!defined('DB_HOST')) {
	define('DB_HOST', 'localhost');
}

/** Database Charset to use in creating database tables. */
if (!defined('DB_CHARSET')) {
	define('DB_CHARSET', 'utf8');
}

/** The Database Collate type. Don't change this if in doubt. */
if (!defined('DB_COLLATE')) {
	define('DB_COLLATE', '');
}

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '@!`kjw&H-TmU)ufE]a$<6ta+jJ(ME<q`]5|b+[6+uOVC5F(Hu[/Qb+:95U,(u_Rg');
define('SECURE_AUTH_KEY',  '*dQdzg&m;SY {Y+9!jP}[|BT79_W3iR?J*t+>?lgN+A/5[-=SMWIOn+EJ=:)TA|%');
define('LOGGED_IN_KEY',    'uAzzEYV@aQ7sxYVvKphpE~|efym Bf+Lt(1I> UDvrz`B9.R0m+r<{M*WU:_NFb7');
define('NONCE_KEY',        '+!0Gbm]iC4D-3P+IYPlSW1rn$H,S^sAf05^7.SwFD^$Vs?iE0w+>Xv#h_p;!8Bk=');
define('AUTH_SALT',        '2v>fw$ndS5t.^H@9<V[q5$uL00sesU+6d=3d4vmsHBvz_LUBy5?VoMgF1}e(=BYv');
define('SECURE_AUTH_SALT', 'hmX.a-l)0$+ZE[>4>C#V?FrL`3e,8UEg){>dmtQCP*qELhQW,,=/8TY +`?$eDW3');
define('LOGGED_IN_SALT',   '`E.;[P$3^HMa+BsYi-BY*GI>t[#<$-AQci-r=r+xj(V1?U=V4)Px-|tI l+m K7K');
define('NONCE_SALT',       '<7=w&UTm}u+[^J*}{2Ww R/[>iVDn9ZO`bVLy5!NvePC-Sq(T<) Dp_o${jI&y)!');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');



/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
if (!defined('WP_DEBUG')) {
	define('WP_DEBUG', false);
}

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
